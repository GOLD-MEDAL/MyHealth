package com.itheima.health.job;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.service.OrderSettingService;
import com.itheima.health.utils.DateUtils;
import org.springframework.stereotype.Component;

/**
 * @author: zzk
 * @date: 2019/12/2
 * @darctiption: com.itzzk.health.job
 */
@Component
public class ClearBackOrderSettingJob {

    @Reference
    private OrderSettingService orderSettingService;

    /**
     * 定时清除过期的预约设置
     */
    public void clearBackOrderSetting(){
        try {
            String backDate = DateUtils.parseDate2String(DateUtils.getLastMonthLastDayByThisMonth());
            if (backDate != null && backDate.length() > 0) {
                orderSettingService.deleteBackOrderSetting(backDate);
            }
            System.out.println("过期预约设置已经被删除了哦~~~");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
