package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.User;

public interface UserService {

    User findUserByUsername(String username);


    void add(User user);

    User findById(Integer id);

    PageResult findPage(QueryPageBean queryPageBean);

    void delete(Integer id);

    void edit(User user);
}
