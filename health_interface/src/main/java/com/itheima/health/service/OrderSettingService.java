package com.itheima.health.service;

import com.itheima.health.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSettingService {

    void addList(List<OrderSetting> orderSettingsList);

    List<Map> findOrderSettingByOrderDateMonth(String date);

    void updateOrderSettingByOrderDate(OrderSetting orderSetting);

    /**
     * 定时清除过期的预约设置
     * @param backDate
     */
    void deleteBackOrderSetting(String backDate);
}
