package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Menu;

import java.util.List;

public interface MenuService {

    void add(Menu menu);

    PageResult findPage(QueryPageBean queryPageBean);

//    获取动态菜单
    List<Menu> getDynamicMenu(String username);
    void delete(Integer id);

    Menu findById(Integer id);

    void edit(Menu menu);

    List<Menu> findParentMenu();

    List<Menu> findAll();

    List<Integer> findMenuListByRoleId(Integer roleId);


}
