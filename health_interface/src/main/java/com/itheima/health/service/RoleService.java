package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Role;

import java.util.List;

public interface RoleService {

    void add(Role role, Integer[] permissionsId, Integer[] menusId);

    PageResult findPage(QueryPageBean queryPageBean);

    Role findById(Integer id);

    void edit(Role role, Integer[] permissionsId, Integer[] menusId);

    void delete(Integer id);

    List<Role> findAll();


    List<Integer> findRoleListByRoleId(Integer roleId);
}
