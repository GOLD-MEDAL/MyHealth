package com.w.health.service;

import com.w.health.domain.CheckGroup;
import com.w.health.entity.PageResult;

import java.util.List;

public interface CheckGroupService {

	void add(CheckGroup checkGroup, Integer[] checkitemIds);

	PageResult QueryPage(Integer currentPage, Integer pageSize, String queryString);

	CheckGroup findById(Integer id);

	List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

	void edit(CheckGroup checkGroup, Integer[] checkitemIds);

	List<CheckGroup> findAll();
}
