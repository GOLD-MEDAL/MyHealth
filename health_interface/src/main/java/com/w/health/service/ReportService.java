package com.w.health.service;

import java.util.Map;

public interface ReportService {
	Map<String,Object> getBusinessReport() throws Exception;

}
