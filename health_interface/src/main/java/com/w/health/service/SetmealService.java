package com.w.health.service;

import com.w.health.domain.Setmeal;
import com.w.health.entity.PageResult;

import java.util.List;
import java.util.Map;

public interface SetmealService {
	void add(Setmeal setmeal, Integer[] checkgroupIds);

	PageResult findPage(Integer currentPage, Integer pageSize, String queryString);

	List<Setmeal> findAll();

	Setmeal findById(Integer id);

	List<Map<String,Object>> findSetmealCount();
}
