package com.w.health.service;

import com.w.health.domain.OrderSetting;

import java.util.List;
import java.util.Map;

public interface OrderSettingSerivce {
	void add(List<OrderSetting> orderSettingList);

	List<Map> getOrderSettingByMonth(String date);

	void editNumberByDate(OrderSetting orderSetting);
}
