package com.w.health.service;

import com.w.health.domain.CheckGroup;
import com.w.health.domain.CheckItem;
import com.w.health.entity.PageResult;

import java.util.List;

public interface CheckItemService {

	void add(CheckItem checkItem);

	PageResult pageQuery(Integer currentPage,Integer pageSize,String queryString);

	void delete(Integer id);

	CheckItem findById(Integer id);

	void edit(CheckItem checkItem);

	/*==============查询所有==================*/
	List<CheckItem> findAll();
}
