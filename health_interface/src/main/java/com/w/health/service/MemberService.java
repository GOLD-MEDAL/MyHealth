package com.w.health.service;

import com.w.health.domain.Member;

import java.util.List;
import java.util.Map;

public interface MemberService {
	void add(Member member);

	Member findByTelephone(String telephone);

	List<Integer> findMemberCountByMonth(List<String> list);


	List<Map<String,Object>> findMemberCount();
}
