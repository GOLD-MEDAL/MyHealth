package com.w.health.service;


import com.w.health.domain.User;

public interface UserService {

	User findUserByUsername(String username);
}
