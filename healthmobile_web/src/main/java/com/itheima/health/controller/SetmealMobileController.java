package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName SetmealMobileController
 * @Description TODO
 * @Author ly
 * @Company 深圳黑马程序员
 * @Date 2019/11/25 15:40
 * @Version V1.0
 */
@RestController
@RequestMapping(value = "/setmeal")
public class SetmealMobileController {

    @Reference
    SetmealService setmealService;
    @Autowired
    JedisPool jedisPool;

    ObjectMapper objectMapper = new ObjectMapper();
    // 查询所有的套餐
    @RequestMapping(value = "/getSetmeal")
    public Result getSetmeal() {


        try {
            //String asString = objectMapper.writeValueAsString(list);

            String get = jedisPool.getResource().get("asString");

            if (get != null ) {
                return new Result(true, MessageConstant.GET_SETMEAL_LIST_SUCCESS, get);
            } else {
                List<Setmeal> list = setmealService.findAll();
                if (list!=null){
                    return new Result(true, MessageConstant.GET_SETMEAL_LIST_SUCCESS,list);
                }else {
                    return new Result(false, MessageConstant.GET_SETMEAL_LIST_FAIL);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

            return new Result(false, MessageConstant.GET_SETMEAL_LIST_FAIL);
        }
    }

    // 使用套餐id，查询套餐id所对应的套餐信息（详情）
    @RequestMapping(value = "/findById")
    public Result findById(Integer id){
        Setmeal setmeal = setmealService.findById(id);

        try {
            String obs = objectMapper.writeValueAsString(setmeal);
            jedisPool.getResource().set("setmeal"+id, obs);
            if(setmeal.getId().equals(id)){
                String jedisResource = jedisPool.getResource().get("setmeal" + id);
                String[] jrs = jedisResource.split(",");
                List<String> list = Arrays.asList(jrs);
                if (list!=null){
                    return new Result(true, MessageConstant.QUERY_SETMEAL_SUCCESS,list);
                }else {
                    return new Result(true,MessageConstant.QUERY_SETMEAL_FAIL,setmeal);
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();

            return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);
        }
        return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);

    }
}
