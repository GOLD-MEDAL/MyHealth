package com.w.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.w.health.constants.MessageConstant;
import com.w.health.constants.RedisConstant;
import com.w.health.constants.RedisMessageConstant;
import com.w.health.domain.Order;
import com.w.health.entity.Result;
import com.w.health.service.OrderService;
import com.w.utils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderMobileController {
	@Reference
	private OrderService orderService;

	@Autowired
	private JedisPool jedisPool;

	/**
	 * 体检预约
	 */
	@RequestMapping("/submit")
	public Result submitOrder(@RequestBody Map map){
		String telephone = (String) map.get("telephone");
		//从redis中获取缓存的验证码,KEY为手机号+RedisConstant.SENDTYPE_ORDER
		String codeInRedis = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_ORDER);
		String validateCode = (String) map.get("validateCode");
		//校验手机验证码
		if (codeInRedis==null&&!codeInRedis.equals(validateCode)){
			return new Result(false, MessageConstant.VALIDATECODE_ERROR);
		}
		//调用体验预约服务
		Result result = null;

		try {
			map.put("orderType", Order.ORDERTYPE_WEIXIN);
			result = orderService.order(map);
		} catch (Exception e) {
			e.printStackTrace();
			//预约失败
			return result;
		}
		if(result.isFlag()){
			//预约成功发送短信通知,短信通知内容可以是"预约时间","预约人","预约地点","预约事项"等信息
			String orderDate = (String) map.get("orderDate");
			try {
				SMSUtils.sendShortMessage(telephone,orderDate);
			} catch (ClientException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	/**
	 * 根据id查询预约信息,包括套餐信息和会员信息
	 */
	@RequestMapping("/findById")
	public Result findById(Integer id){
		Map map = null;
		try {
			map = orderService.findById4Detail(id);
			return new Result(true,MessageConstant.QUERY_ORDER_SUCCESS,map);
		} catch (Exception e) {
			e.printStackTrace();
			//查询预约信息失败
			return new Result(false,MessageConstant.QUERY_ORDER_FAIL);
		}
	}
}
