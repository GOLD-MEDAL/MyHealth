package com.w.health.controller;


import com.w.health.constants.MessageConstant;
import com.w.health.constants.RedisMessageConstant;
import com.w.health.entity.Result;
import com.w.utils.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {

	@Autowired
	JedisPool jedisPool;

	/*
		体检预约时发送手机验证码
	*/
	@RequestMapping("/send4Order")
	public Result send4Order(String telephone){


			//发送信息
			//SMSUtils.sendShortMessage(code.toString(),telephone);
			Integer code = ValidateCodeUtils.generateValidateCode(4);//生成4位数验证码

		//将生成的验证码缓存到redis中
		jedisPool.getResource().setex(telephone+ RedisMessageConstant.SENDTYPE_ORDER,5*60,code.toString());
		//验证码发送成功
		return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
	}
	/**
	 * 手机快速登陆时发送手机验证码
	 */
	@RequestMapping("/send4Login")
	public Result send4Login(String telephone){
		//生成4位数验证码
		Integer code = ValidateCodeUtils.generateValidateCode(4);
		/*//发送短信
		try {
			SMSUtils.sendShortMessage(telephone,code.toString());
		} catch (ClientException e) {
			e.printStackTrace();
			//验证码发送失败
			return new Result(false,MessageConstant.SEND_VALIDATECODE_FAIL);
		}*/
		System.out.println("发送手机的验证码:" + code);
		//将生成的验证码存到redis中
		jedisPool.getResource().setex(telephone+ RedisMessageConstant.SENDTYPE_LOGIN,5*60,code.toString());
		//验证码发送成功
		return new Result(true,MessageConstant.SEND_VALIDATECODE_SUCCESS);
	}
}
