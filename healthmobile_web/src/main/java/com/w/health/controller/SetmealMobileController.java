package com.w.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.w.health.constants.MessageConstant;
import com.w.health.domain.Setmeal;
import com.w.health.entity.Result;
import com.w.health.service.SetmealService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/setmealMobile")
public class SetmealMobileController {
	@Reference
	private SetmealService setmealService;

	@RequestMapping("/getSetmeal")
	public Result getSetmeal(){
		try {
			List<Setmeal> list = setmealService.findAll();
			return new Result(true, MessageConstant.GET_SETMEAL_LIST_SUCCESS,list);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.GET_SETMEAL_LIST_FAIL);

		}
	}

	@RequestMapping("/findById")
	public Result findById(Integer id){
		try {
			Setmeal setmeal = setmealService.findById(id);
			return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,setmeal);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);
		}

	}
}
