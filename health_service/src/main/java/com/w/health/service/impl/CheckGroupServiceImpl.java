package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.w.health.dao.CheckGroupDao;
import com.w.health.domain.CheckGroup;
import com.w.health.entity.PageResult;
import com.w.health.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CheckGroupServiceImpl implements CheckGroupService{

	@Autowired
	private CheckGroupDao checkGroupDao;

	@Override
	public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
		checkGroupDao.add(checkGroup);

		setCheckGroupAndCheckItem(checkGroup.getId(),checkitemIds);
	}

	@Override
	public PageResult QueryPage(Integer currentPage, Integer pageSize, String queryString) {
		PageHelper.startPage(currentPage,pageSize);
		Page<CheckGroup> page = checkGroupDao.findPage(queryString);
		return new PageResult(page.getTotal(),page.getResult());
	}

	@Override
	public CheckGroup findById(Integer id) {
		return checkGroupDao.findById(id);
	}

	@Override
	public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
		List<Integer> list = checkGroupDao.findCheckItemIdsByCheckGroupId(id);
		return list;
	}

	@Override
	public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
		//根据中间表删除中间报数据
		checkGroupDao.deleteAssociation(checkGroup.getId());
		//向中间表插入数据
		setCheckGroupAndCheckItem(checkGroup.getId(),checkitemIds);
		//更新检查组基本信息
		checkGroupDao.edit(checkGroup);
	}

	@Override
	public List<CheckGroup> findAll() {
		return checkGroupDao.findAll();
	}

	private void setCheckGroupAndCheckItem(Integer checkGroupId, Integer[] checkitemIds) {
		if (checkitemIds!=null && checkitemIds.length>0){
			for (Integer id : checkitemIds) {
				Map<String,Integer> map = new HashMap<>();
				map.put("checkgroup_id",checkGroupId);
				map.put("checkitem_id",id);
				checkGroupDao.setCheckGroupAndCheckItem(map);
			}
		}
	}
}
