package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.w.health.constants.MessageConstant;
import com.w.health.dao.MemberDao;
import com.w.health.dao.OrderDao;
import com.w.health.dao.OrderSettingDao;
import com.w.health.domain.Member;
import com.w.health.domain.Order;
import com.w.health.domain.OrderSetting;
import com.w.health.entity.Result;
import com.w.health.service.OrderService;
import com.w.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private OrderSettingDao orderSettingDao;
	/**
	 * 体检预约
	 */
	@Override
	public Result order(Map map) throws Exception {
		//检查当前日期是否进行了预约设置
		String OrderDate = (String) map.get("orderDate");
		Date date = DateUtils.parseString2Date(OrderDate);
		OrderSetting orderSetting = orderSettingDao.findByOrderDate(date);
		if (orderSetting == null){
			return new Result(false, MessageConstant.SELECTED_DATE_CANNOT_ORDER);
		}
		//检查预约日期是否预约已满
		int number = orderSetting.getNumber();//可预约人数
		int reservations = orderSetting.getReservations();//已预约人数
		if (reservations>=number){
			//预约已满不能预约
			return new Result(false,MessageConstant.ORDER_FULL);
		}
		//检查当前用户是否为会员,根据手机号判断
		String telephone = (String) map.get("telephone");
		Member member = memberDao.findByTelephone(telephone);
		//如果是会员,防止重复预约
		if (member != null){
			Integer memberId = member.getId();
			int setmealId = Integer.parseInt((String)map.get("setmealId"));
			Order order = new Order(memberId,date,null,null,setmealId);
			List<Order> list = orderDao.findByCondition();
			if (list != null && list.size()>0){
				//已经完成了预约,不能重复预约
				return  new Result(false,MessageConstant.HAS_ORDERED);
			}
		}
		//可以预约,设置预约人数+1
		orderSettingDao.editReservationsByOrderDate(date);

		if (member == null){
			//当前用户不是会员,需要添加到会员表
			member = new Member();
			member.setName((String) map.get("name"));
			member.setPhoneNumber((String) map.get("telephone"));
			member.setIdCard((String) map.get("idCard"));
			member.setSex((String) map.get("sex"));
			member.setRegTime(new Date());
			memberDao.add(member);
		}
		//保存预约信息到预约表
		Order order = new Order(member.getId(),date,(String)map.get("orderType"),Order.ORDERSTATUS_NO,Integer.parseInt((String)map.get("setmealId")));
		orderDao.add(order);
		return new Result(true,MessageConstant.ORDER_SUCCESS,order);
	}

	@Override
	public Map findById4Detail(Integer id) {
		Map map = orderDao.findById4Detail(id);
		if (map!=null){
			Date orderDate = (Date) map.get("orderDate");
			return map;
		}
		return map;
	}
}
