package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.w.health.dao.CheckItemDao;
import com.w.health.domain.CheckGroup;
import com.w.health.domain.CheckItem;
import com.w.health.entity.PageResult;
import com.w.health.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class CheckItemServiceImpl implements CheckItemService{

	@Autowired
	private CheckItemDao checkItemDao;

	//新增检查项
	@Override
	public void add(CheckItem checkItem) {
		checkItemDao.add(checkItem);
	}

	//分页查询
	@Override
	public PageResult pageQuery(Integer currentPage,Integer pageSize,String queryString) {
		PageHelper.startPage(currentPage,pageSize);
		Page<CheckItem> checkItems = checkItemDao.selectByCondition(queryString);
		return new PageResult(checkItems.getTotal(),checkItems.getResult());
	}

	//删除检查项
	@Override
	public void delete(Integer id) throws RuntimeException {

		Long count = checkItemDao.findCountByCheckItemId(id);
		if (count>0){
			//当前检查项被引用,不能删除
			throw new RuntimeException("当前检查项被检查组引用,不能删除");
		}
		checkItemDao.delete(id);
	}

	@Override
	public CheckItem findById(Integer id) {
		return checkItemDao.findById(id);
	}

	@Override
	public void edit(CheckItem checkItem) {
		checkItemDao.edit(checkItem);
	}

	@Override
	public List<CheckItem> findAll() {
		List<CheckItem> list = checkItemDao.findAll();
		return list;
	}


}
