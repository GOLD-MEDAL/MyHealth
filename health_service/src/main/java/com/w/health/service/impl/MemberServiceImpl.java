package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.w.health.dao.MemberDao;
import com.w.health.domain.Member;
import com.w.health.service.MemberService;
import com.w.utils.DateUtils;
import com.w.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberServiceImpl implements MemberService {
	@Autowired
	private MemberDao memberDao;
	@Override
	public Member findByTelephone(String telephone) {
		return memberDao.findByTelephone(telephone);
	}

	/**
	 * 根据月份统计会员数量
	 * @param
	 * @return
	 */
	// 根据月份统计会员数量
	public List<Integer> findMemberCountByMonth(List<String> months) {
		List<Integer> list = new ArrayList<>();
		for(String m : months){
			m = m + "-31";//格式：2019-04-31
			Integer count = memberDao.findMemberCountBeforeDate(m);
			list.add(count);
		}
		return list;
	}

	@Override
	public List<Map<String, Object>> findMemberCount() {
		try {
			String today = DateUtils.parseDate2String(DateUtils.getToday());
			String[] date = today.split("-");
			List<Member> members = memberDao.findMember();
			Map<String,Object> map1 = new HashMap<>();
			Map<String,Object> map2 = new HashMap<>();
			Map<String,Object> map3 = new HashMap<>();
			Map<String,Object> map4 = new HashMap<>();
			Integer sn = 0;//0-18岁
			Integer qn = 0;//18-30岁
			Integer zn = 0;//30-45岁
			Integer ln = 0;//45岁以上
			for (Member member : members) {
				String[] birthday = DateUtils.parseDate2String(member.getBirthday()).split("-");
				if (Integer.parseInt(date[0])-Integer.parseInt((birthday[0])) <= 18){
					sn++;

				}else if (Integer.parseInt(date[0])-Integer.parseInt((birthday[0])) > 18 && Integer.parseInt(date[0])-Integer.parseInt((birthday[0]))<=30){
					qn++;

				}else if (Integer.parseInt(date[0])-Integer.parseInt((birthday[0]))>30 && Integer.parseInt(date[0])-Integer.parseInt((birthday[0]))<=45){
					zn++;

				}else {
					ln++;

				};
			}
			map1.put("name","0-18岁");
			map1.put("value",sn);
			map2.put("name","18-30岁");
			map2.put("value",qn);
			map3.put("name","30-45");
			map3.put("value",zn);
			map4.put("name","45以上");
			map4.put("value",ln);
			List<Map<String,Object>> list = new ArrayList<>();
			list.add(map1);
			list.add(map2);
			list.add(map3);
			list.add(map4);
			return list;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public void add(Member member) {
		if (member.getPassword() != null){
			member.setPassword(MD5Utils.md5(member.getPassword()));
		}
		memberDao.add(member);
	}


}
