package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.w.health.dao.UserDao;
import com.w.health.domain.User;
import com.w.health.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;
	@Override
	public User findUserByUsername(String username) {
		User user = userDao.findUserByUsername(username);
		return user;
	}
}
