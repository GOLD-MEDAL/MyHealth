package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.w.health.constants.RedisConstant;
import com.w.health.dao.SetmealDao;
import com.w.health.domain.Setmeal;
import com.w.health.entity.PageResult;
import com.w.health.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = SetmealService.class)
@Transactional
public class SetmealServiceImpl implements SetmealService{
	@Autowired
	private SetmealDao setmealDao;
	private JedisPool jedisPool;

	@Override
	public void add(Setmeal setmeal, Integer[] checkgroupIds) {
		setmealDao.add(setmeal);

		//向中间表中插入数据
		if (checkgroupIds !=null && checkgroupIds.length>0){
			setSetMealAndCheckGroup(setmeal.getId(),checkgroupIds);
		}
		jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES,setmeal.getImg());

	}

	@Override
	public PageResult findPage(Integer currentPage, Integer pageSize, String queryString) {
		PageHelper.startPage(currentPage,pageSize);
		Page<Setmeal> page = setmealDao.queryPage(queryString);
		return new PageResult(page.getTotal(),page.getResult());
	}

	@Override
	public List<Setmeal> findAll() {
		List<Setmeal> list = setmealDao.findAll();
		return list;
	}

	@Override
	public Setmeal findById(Integer id) {
		Setmeal setmeal = setmealDao.findById(id);
		return setmeal;
	}

	@Override
	public List<Map<String, Object>> findSetmealCount() {
		return setmealDao.findSetmealCount();
	}

	private void setSetMealAndCheckGroup(Integer id, Integer[] checkgroupIds) {
		for (Integer checkgroupId : checkgroupIds) {
			Map<String ,Integer> map = new HashMap<>();
			map.put("setmeal_id",id);
			map.put("checkgroup_id",checkgroupId);
			setmealDao.setSetmealAndCheckGroup(map);
		}
	}
}
