package com.w.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.w.health.dao.OrderSettingDao;
import com.w.health.domain.OrderSetting;
import com.w.health.service.OrderSettingSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = OrderSettingSerivce.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingSerivce {
	@Autowired
	private OrderSettingDao orderSettingDao;
	@Override
	public void add(List<OrderSetting> list) {
		if (list!=null && list.size()>0){
			for (OrderSetting orderSetting : list) {
				long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
				if (count>0){
					//已经存在执行更新操作
					orderSettingDao.editNumberOrderDate(orderSetting);
				}else {
					//不存在执行添加操作
					orderSettingDao.add(orderSetting);
				}
			}
		}
	}

	@Override
	public List<Map> getOrderSettingByMonth(String date) {//2019-03
		// 1.组织查询Map，dateBegin表示月份开始时间，dateEnd月份结束时间
		String dateBegin = date + "-1";//2019-03-1
		String dateEnd = date + "-31";//2019-03-31
		Map map = new HashMap();
		map.put("dateBegin",dateBegin);
		map.put("dateEnd",dateEnd);
		// 2.查询当前月份的预约设置
		List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(map);
		List<Map> data = new ArrayList<>();
		// 3.将List<OrderSetting>，组织成List<Map>
		for (OrderSetting orderSetting : list) {
			Map orderSettingMap = new HashMap();
			orderSettingMap.put("date",orderSetting.getOrderDate().getDate());//获得日期（几号）
			orderSettingMap.put("number",orderSetting.getNumber());//可预约人数
			orderSettingMap.put("reservations",orderSetting.getReservations());//已预约人数
			data.add(orderSettingMap);
		}
		return data;
	}

	@Override
	public void editNumberByDate(OrderSetting orderSetting) {
		long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
		if (count>0){
			//当前日期已经尽心那个了预约设置需要进行修改操作
			orderSettingDao.editNumberByOrderDate(orderSetting);
		}else{
			//当前日期没有进行预约设置,惊醒添加操作
			orderSettingDao.add(orderSetting);
		}
	}
}
