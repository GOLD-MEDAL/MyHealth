package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.RoleDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    @Override
    public void add(Role role, Integer[] permissionsId, Integer[] menusId) {
        //添加角色
        int rows = roleDao.addRole(role);
        if (rows > 0 && permissionsId != null && permissionsId.length > 0){
            //添加role和permission中间表
            Map<String,Object> map = new HashMap<>();
            map.put("roleId",role.getId());
            map.put("permissionsId",permissionsId);
            map.put("menusId",menusId);
            roleDao.addPermissionAndRole(map);
            roleDao.addMenuAndRole(map);
        }
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<Role> page= roleDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public Role findById(Integer id) {

        return roleDao.findById(id);
    }

    @Override
    public void edit(Role role, Integer[] permissionsId,Integer[] menusId){
        //删除角色和权限外键关联
        roleDao.deletePermissionAndRole(role.getId());
        //删除角色和菜单外键关联
        roleDao.deleteMenuAndRole(role.getId());
        //修改role信息
        roleDao.edit(role);
        //增加角色和权限外键关系
        Map<String, Object> permissionAndRoleMap = new HashMap<>();
        permissionAndRoleMap.put("roleId",role.getId());
        permissionAndRoleMap.put("permissionsId",permissionsId);
        roleDao.addPermissionAndRole(permissionAndRoleMap);
        //增加角色和菜单外键关系
        Map<String, Object> menuAndRoleMap = new HashMap<>();
        menuAndRoleMap.put("roleId",role.getId());
        menuAndRoleMap.put("menusId",menusId);
        roleDao.addMenuAndRole(menuAndRoleMap);
    }

    @Override
    //删除角色
    public void delete(Integer id) {
        //删除权限和角色中间表
        roleDao.deletePermissionAndRole(id);
        //删除角色和用户中间表
        roleDao.deleteUserAndRole(id);
        roleDao.delete(id);
    }

    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }

    @Override
    public List<Integer> findRoleListByRoleId(Integer roleId) {
        return roleDao.findRoleListByRoleId(roleId);
    }


}
