package com.itheima.health.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.MenuDao;
import com.itheima.health.dao.RoleDao;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.UserDao;
import com.itheima.health.pojo.Menu;
import com.itheima.health.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.itheima.health.pojo.Menu;
import com.itheima.health.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.ArrayList;
import java.util.List;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = MenuService.class)
@Transactional
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuDao menuDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private MenuDao menuDao;

    @Autowired
    private RoleDao roleDao;

    @Override
    public List<Menu> getDynamicMenu(String username) {
        List<Integer> roleIds = userDao.getRoleIdsByUsername(username);
        if (roleIds.size()>0 && roleIds != null){
                List<Integer> menuIds = roleDao.getMenuIdsByRoleId(roleIds);//通过roleId查找menuId的集合
//                通过menuId查找一级菜单
                List<Menu> levelOne = menuDao.findLevelOneMenuBymenuId(menuIds);
            for (Menu menu : levelOne) {
                Integer parentId = menu.getId();
//                HashMap<String, Object> paramMap = new HashMap<>();
//                paramMap.put("parentId",parentId);
//                paramMap.put("menuIds",menuIds);
                List<Menu> childrenMenu = new ArrayList<>();
                for (Integer menuId : menuIds) {
//                    查找一级菜单下的子菜单
                        Menu childMenu = menuDao.findLevelChildrenMenuBymenuId(parentId,menuId);
//                        不为空则添加到集合内,最后
                        if(childMenu != null)
                        childrenMenu.add(childMenu);

                }
//                遍历完单个一级菜单后添加到一级菜单内的children集合
                menu.setChildren(childrenMenu);
            }
            return levelOne;
        }
        return null;
    }
@Override
    public void add(Menu menu) {
        int level = 1;
        if (menu.getParentMenuId() != null) {
            level = 2;
            //根据父菜单Id从数据库中查询父菜单path
            Integer parentPath = menuDao.findParentPath(menu.getParentMenuId());
            //设置路由路径
            //获取最大优先级
            Integer maxChildrenPriority = menuDao.getMaxChildrenPriority(menu.getParentMenuId());
            menu.setPath("/"+ parentPath+"-"+(maxChildrenPriority+1));
            //设置优先级
            menu.setPriority(maxChildrenPriority+1);
        }else{
            //父菜单最大路径id
            Integer maxParentId = menuDao.findMaxParentId();
            //设置路由路径
            menu.setPath(String.valueOf(maxParentId+1));
            //设置优先级
            // 根据父菜单Id获取最大优先级
            Integer maxPriority = menuDao.getMaxParentPriority(maxParentId);
            menu.setPriority(1+maxPriority);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("menu", menu);
        map.put("level", level);
        menuDao.add(map);
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<Menu> page = menuDao.findPage(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void delete(Integer id) {
        menuDao.delete(id);
    }

    @Override
    public Menu findById(Integer id) {
        Menu menu = menuDao.findById(id);
        return menu;
    }

    @Override
    public void edit(Menu menu) {
        int level = 1;
        if (menu.getParentMenuId() != null) {
            level = 2;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("menu",menu);
        map.put("level",level);
        menuDao.edit(map);
    }

    @Override
    public List<Menu> findParentMenu() {
        return menuDao.findParentMenu();
    }

    @Override
    public List<Menu> findAll() {
        return menuDao.findAll();
    }

    @Override
    public List<Integer> findMenuListByRoleId(Integer roleId) {
        return menuDao.findMenuListByRoleId(roleId);
    }


}
