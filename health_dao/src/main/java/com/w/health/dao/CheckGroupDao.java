package com.w.health.dao;


import com.github.pagehelper.Page;
import com.w.health.domain.CheckGroup;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CheckGroupDao {


	void add(CheckGroup checkGroup);

	void setCheckGroupAndCheckItem(Map<String, Integer> map);

	Page<CheckGroup> findPage(String queryString);

	CheckGroup findById(Integer id);

	List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

	void deleteAssociation(Integer id);

	void edit(CheckGroup checkGroup);

	List<CheckGroup> findAll();

	List<CheckGroup> findCheckGroupListById(Integer id);
}
