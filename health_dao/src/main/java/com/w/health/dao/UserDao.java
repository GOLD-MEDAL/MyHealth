package com.w.health.dao;

import com.w.health.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao {
	User findUserByUsername(String username);
}
