package com.w.health.dao;

import com.w.health.domain.Permission;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PermissionDao {
	Set<Permission> findPermissionsByRoleId(Integer roleId);
}
