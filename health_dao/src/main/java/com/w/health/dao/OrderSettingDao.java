package com.w.health.dao;

import com.w.health.domain.OrderSetting;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface OrderSettingDao {
	long findCountByOrderDate(Date orderDate);

	void editNumberOrderDate(OrderSetting orderSetting);

	void add(OrderSetting orderSetting);

	List<OrderSetting> getOrderSettingByMonth(Map map);

	void editNumberByOrderDate(OrderSetting orderSetting);

	OrderSetting findByOrderDate(Date date);

	void editReservationsByOrderDate(Date date);
}
