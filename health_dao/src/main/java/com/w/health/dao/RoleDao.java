package com.w.health.dao;

import com.w.health.domain.Role;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RoleDao {
	Set<Role> findRolesByUserId(Integer userId);

}
