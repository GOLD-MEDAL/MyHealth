package com.w.health.dao;

import com.github.pagehelper.Page;
import com.w.health.domain.CheckItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CheckItemDao {

	void add(CheckItem checkItem);

	Page<CheckItem> selectByCondition(String queryString);

	void delete(Integer id);

	long findCountByCheckItemId(Integer id);

	CheckItem findById(Integer id);

	void edit(CheckItem checkItem);

	/*===================查询所有================*/
	List<CheckItem> findAll();

	List<CheckItem> findCheckItemListById(Integer id);
}
