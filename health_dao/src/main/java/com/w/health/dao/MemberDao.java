package com.w.health.dao;

import com.w.health.domain.Member;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemberDao {
	Member findByTelephone(String telephone);

	void add(Member member);

	Integer findMemberCountBeforeDate(String m);

	Integer findMemberCountAfterDate(String thisWeekMonday);

	Integer findMemberTotalCount();

	Integer findMemberCountByDate(String date);


	List<Member> findMember();
}
