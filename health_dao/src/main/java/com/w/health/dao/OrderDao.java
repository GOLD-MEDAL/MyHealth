package com.w.health.dao;

import com.w.health.domain.Order;

import java.util.List;
import java.util.Map;

public interface OrderDao {
	List<Order> findByCondition();

	void add(Order order);

	Map findById4Detail(Integer id);

	Integer findOrderCountByDate(String date);

	Integer findOrderCountBetweenDate(Map<String, Object> weekMap);

	Integer findVisitsCountByDate(String date);

	Integer findVisitsCountAfterDate(Map<String, Object> weekMap2);

	List<Map> findHotSetmeal();
}
