package com.w.health.dao;

import com.github.pagehelper.Page;
import com.w.health.domain.Setmeal;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SetmealDao {


	void add(Setmeal setmeal);

	void setSetmealAndCheckGroup(Map<String, Integer> map);

	Page<Setmeal> queryPage(String queryString);

	List<Setmeal> findAll();

	Setmeal findById(Integer id);

	List<Map<String,Object>> findSetmealCount();

}
