package com.itheima.health.dao;
import com.github.pagehelper.Page;
import com.itheima.health.pojo.Role;

import java.util.List;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RoleDao {

    Set<Role> findRolesByUserId(Integer userId);

    //    通过roleId查找menuId的集合
    List<Integer> getMenuIdsByRoleId(List roleIds);

    Integer addRole(Role role);

    void addPermissionAndRole(Map<String, Object> map);

    Page<Role> findPage(String queryString);

    Role findById(Integer id);

    void deletePermissionAndRole(Integer id);

    void deleteMenuAndRole(Integer id);

    void delete(Integer id);

    void deleteUserAndRole(Integer id);

    void addMenuAndRole(Map<String, Object> map);

    List<Role> findAll();

    List<Integer> findRoleListByRoleId(Integer roleId);

    void edit(Role role);
}
