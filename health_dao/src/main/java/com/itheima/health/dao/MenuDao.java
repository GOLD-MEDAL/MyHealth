package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MenuDao {

    void add(Map map);

    Page<Menu> findPage(String queryString);

    void delete(Integer id);

    Menu findById(Integer id);

    void edit(Map<String, Object> map);

    List<Menu> findParentMenu();

    List<Menu> findAll();

    List<Integer> findMenuListByRoleId(Integer roleId);

    Integer findMaxParentId();

    Integer getMaxChildrenPriority(Integer parentMenuId);

    Integer getMaxParentPriority(Integer parentMenuId);

    Integer findParentPath(Integer parentMenuId);

	//通过menuId查找一级菜单
    List<Menu> findLevelOneMenuBymenuId(List<Integer> menuIds);
    //通过父菜单的ID和用户的所有子菜单的ID查询子菜单
    Menu findLevelChildrenMenuBymenuId(@Param("parentId")Integer parentId,@Param("menuId") Integer menuId);
}
