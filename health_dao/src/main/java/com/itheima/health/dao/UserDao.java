package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.User;

import java.util.List;

public interface UserDao {

    User findUserByUsername(String username);

    //    获取用户对应的动态菜单
    List<Integer> getRoleIdsByUsername(String username);


    void add(User user);

    User findById(Integer id);


    Page<User> findPage(String queryString);

    void delete(Integer id);

    void edit(User user);
}
