package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.Permission;

import java.util.List;
import java.util.Set;

public interface PermissionDao {

    Set<Permission> findPermissionsByRoleId(Integer roleId);

    void add(Permission permission);

    Page<Permission> findPage(String queryString);

    Permission findById(Integer id);

    void edit(Permission permission);

    void delete(Integer id);

    Integer deletePermissionAndRole(Integer id);

    List<Permission> findAll();

    List<Integer> findPermissionListByRoleId(Integer roleId);
}
