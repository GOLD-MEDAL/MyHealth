package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Permission;
import com.itheima.health.service.PermissionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Reference
    private PermissionService permissionService;

    @RequestMapping("/add")
    public Result add(@RequestBody Permission permission) {
        try {
            permissionService.add(permission);
            return new Result(true, MessageConstant.ADD_PERMISSION_SUCCESS);
        } catch (Exception e) {
            return new Result(false, MessageConstant.ADD_PERMISSION_FAIL);
        }
    }

    @RequestMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {
        try {
            PageResult page = permissionService.findPage(queryPageBean);
            return new Result(true, MessageConstant.QUERY_PERMISSION_SUCCESS, page);
        } catch (Exception e) {
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }

    @RequestMapping("/findById")
    public Result findById(Integer id) {
        try {
            Permission permission = permissionService.findById(id);
            return new Result(true, MessageConstant.QUERY_PERMISSION_SUCCESS, permission);
        } catch (Exception e) {
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }

    @RequestMapping("/edit")
    public Result edit(@RequestBody Permission permission) {
        try {
            permissionService.edit(permission);
            return new Result(true, MessageConstant.EDIT_PERMISSION_SUCCESS);
        } catch (Exception e) {
            return new Result(false, MessageConstant.EDIT_PERMISSION_FAIL);
        }
    }
    //
    @RequestMapping("/delete")
    public Result delete(Integer id) {
        try {
            permissionService.delete(id);
            return new Result(true,MessageConstant.DELETE_PERMISSION_SUCCESS);
        } catch (Exception e) {
            return new Result(true,MessageConstant.DELETE_PERMISSION_SUCCESS);
        }
    }
    @RequestMapping("/findAll")
    public Result findAll() {
        try {
            List<Permission> list = permissionService.findAll();
            return new Result(true,MessageConstant.QUERY_PERMISSION_SUCCESS,list);
        } catch (Exception e) {
            return new Result(false,MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }

    @RequestMapping("/findPermissionListByRoleId")
    public Result findPermissionListByRoleId(Integer roleId) {
        try {
            List<Integer> list = permissionService.findPermissionListByRoleId(roleId);
            return new Result(true,MessageConstant.QUERY_PERMISSION_SUCCESS,list);
        } catch (Exception e) {
            return new Result(false,MessageConstant.QUERY_PERMISSION_FAIL);
        }

    }
}
