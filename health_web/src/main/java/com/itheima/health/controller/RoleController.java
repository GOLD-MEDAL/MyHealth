package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.constant.MessageConstant;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Role;
import com.itheima.health.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Reference
    private RoleService roleService;

    @RequestMapping("/add")
    public Result add(@RequestBody Role role, Integer[] permissionsId,Integer[] menusId){
        try {
            roleService.add(role,permissionsId,menusId);
            return new Result(true, MessageConstant.ADD_ROLE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(true, MessageConstant.ADD_ROLE_SUCCESS);
        }
    }
    @RequestMapping("/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = null;
        try {
            pageResult = roleService.findPage(queryPageBean);
            return new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,pageResult);
        } catch (Exception e) {
            return new Result(false, MessageConstant.QUERY_ROLE_FAIL,pageResult);
        }
    }

    @RequestMapping("/findById")
    public Result findPage(Integer id){
        try {
            Role role = roleService.findById(id);
            return new Result(true,MessageConstant.QUERY_ROLE_SUCCESS,role);
        } catch (Exception e) {
            return new Result(false,MessageConstant.QUERY_ROLE_FAIL);
        }
    }
    @RequestMapping("/edit")
    public Result edit(@RequestBody Role role,Integer[] permissionsId,Integer[] menusId){
        try {
            roleService.edit(role,permissionsId,menusId);
            return new Result(true,MessageConstant.EDIT_ROLE_SUCCESS);
        } catch (Exception e) {
            return new Result(false,MessageConstant.EDIT_ROLE_FAIL);
        }
    }
    @RequestMapping("/delete")
    public Result edit(Integer id){
        try {
            roleService.delete(id);
            return new Result(true,MessageConstant.DELETE_ROLE_SUCCESS);
        } catch (Exception e) {
            return new Result(false,MessageConstant.DELETE_ROLE_FAIL);
        }
    }
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<Role> list = roleService.findAll();
            return new Result(true,MessageConstant.ADD_ROLE_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ADD_ROLE_FAIL);
        }
    }
    //
    @RequestMapping("/findRoleListByRoleId")
    public Result findRoleListByRoleId(Integer roleId){
        try {
            List<Integer> list = roleService.findRoleListByRoleId(roleId);
            return new Result(true,MessageConstant.QUERY_ROLE_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ROLE_FAIL);
        }
    }
}
