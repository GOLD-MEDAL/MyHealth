package com.w.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.w.health.constants.MessageConstant;
import com.w.health.domain.CheckItem;
import com.w.health.entity.PageResult;
import com.w.health.entity.QueryPageBean;
import com.w.health.entity.Result;
import com.w.health.service.CheckItemService;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/checkitem")
public class CheckItemController {
	@Reference
	private CheckItemService checkItemService;

	@RequestMapping("/add")
	@PreAuthorize("hasAuthority('CHECKITEM_ADD')")
	public Result add(@RequestBody CheckItem checkItem) {
		try {
			checkItemService.add(checkItem);
			return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
		} catch (Exception e) {
			return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
		}
	}

	//分页查询
	@RequestMapping("/findPage")
	@PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
	public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
		PageResult pageResult = checkItemService.pageQuery(
		queryPageBean.getCurrentPage(),
		queryPageBean.getPageSize(),
		queryPageBean.getQueryString());
		return pageResult;
	}

	//根据Id删除检查项
	@RequestMapping("/delete")
	@PreAuthorize("hasAuthority('CHECKITEM_DELETE123')")//权限校验，使用CHECKITEM_DELETE123测试
	public Result delete(Integer id){
		try {
			checkItemService.delete(id);
		} catch (RuntimeException e){
			return new Result(false,e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,MessageConstant.DELETE_CHECKGROUP_FAIL);
		}
		return new Result(true,MessageConstant.DELETE_CHECKGROUP_SUCCESS);
	}
	@RequestMapping("/findById")
	public Result findById(Integer id){
		try {
			CheckItem checkItem = checkItemService.findById(id);
			return new Result(true,MessageConstant.QUERY_CHECKITEM_SUCCESS,checkItem);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,MessageConstant.QUERY_CHECKITEM_SUCCESS);
		}
	}
	@RequestMapping("/Edit")
	public Result Edit(@RequestBody CheckItem checkItem){
		try {
			checkItemService.edit(checkItem);
		} catch (Exception e) {
			return new Result(false,MessageConstant.EDIT_CHECKITEM_FAIL);
		}
		return new Result(true,MessageConstant.EDIT_CHECKGROUP_SUCCESS);
	}

	/*============查询所有=============*/
	@RequestMapping("/findAll")
	public Result findAll(){
		List<CheckItem> list = checkItemService.findAll();

		if (list!=null && list.size()>0){
			return new Result(true, MessageConstant.QUERY_CHECKGROUP_SUCCESS,list);
		} else {
			return new Result(false,MessageConstant.QUERY_CHECKGROUP_FAIL);
		}

	}

}

