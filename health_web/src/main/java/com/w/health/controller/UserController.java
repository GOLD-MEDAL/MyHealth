package com.w.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.w.health.constants.MessageConstant;
import com.w.health.entity.Result;
import com.w.health.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/user")
public class UserController {
	@Reference
	private UserService userService;

	/**
	 * 获得当前登录用户的用户名
	 */
	@RequestMapping("/getUsername")
	public Result getUsername(){
		try {
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return new Result(true, MessageConstant.GET_USERNAME_SUCCESS,user.getUsername());
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, MessageConstant.GET_USERNAME_FAIL);

		}
	}


}
