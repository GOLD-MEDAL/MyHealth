package com.w.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.w.health.constants.MessageConstant;
import com.w.health.constants.RedisConstant;
import com.w.health.domain.Setmeal;
import com.w.health.entity.PageResult;
import com.w.health.entity.QueryPageBean;
import com.w.health.entity.Result;
import com.w.health.service.SetmealService;
import com.w.utils.QiNiuUtils;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.util.UUID;
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

	@Reference
	private SetmealService setmealService;
	private JedisPool jedisPool;
	@RequestMapping("/upload")
	public Result upload(@RequestParam("imgFile")MultipartFile imgFile){
		/*try {
			//获取原始文件名
			String originalFilename = imgFile.getOriginalFilename();
			int lastIndexOf = originalFilename.lastIndexOf(".");
			//获取文件后缀
			String suffix = originalFilename.substring(lastIndexOf);

			//使用UUID随机产生文件名称,防止同名文件覆盖
			String fileName = UUID.randomUUID().toString();
			//图片上传成功
			QiNiuUtils.upload2Qiniu(imgFile.getBytes(),fileName);
			return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS);
		} catch (IOException e) {
			e.printStackTrace();
			//图片上传失败
			return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
		}*/
		try{
			//获取原始文件名
			String originalFilename = imgFile.getOriginalFilename();
			int lastIndexOf = originalFilename.lastIndexOf(".");
			//获取文件后缀
			String suffix = originalFilename.substring(lastIndexOf);
			//使用UUID随机产生文件名称，防止同名文件覆盖
			String fileName = UUID.randomUUID().toString() + suffix;
			QiNiuUtils.upload2Qiniu(imgFile.getBytes(),fileName);
			//图片上传成功
			Result result = new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS,fileName);
			//将上传图片名称存入Redis，基于Redis的Set集合存储
			jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,fileName);
			return result;
		}catch (Exception e){
			e.printStackTrace();
			//图片上传失败
			return new Result(false,MessageConstant.PIC_UPLOAD_FAIL);
		}

	}

	@RequestMapping("/add")
	public Result add(@RequestBody Setmeal setmeal,Integer[] checkgroupIds){
		try {
			setmealService.add(setmeal,checkgroupIds);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,MessageConstant.ADD_CHECKGROUP_FAIL);
		}
		return new Result(true,MessageConstant.ADD_CHECKGROUP_SUCCESS);
	}

	@RequestMapping("/findPage")
	public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
		return  setmealService.findPage(
				queryPageBean.getCurrentPage(),
				queryPageBean.getPageSize(),
				queryPageBean.getQueryString()
		);
	}
}
