package com.w.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.w.health.constants.MessageConstant;
import com.w.health.domain.CheckGroup;
import com.w.health.entity.PageResult;
import com.w.health.entity.QueryPageBean;
import com.w.health.entity.Result;
import com.w.health.service.CheckGroupService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/checkgroup")
public class CheckGroupController {

	@Reference
	private CheckGroupService checkGroupService;
	/*==================新增检查组=======================*/
	@RequestMapping("/add")
	public Result add (@RequestBody CheckGroup checkGroup, Integer[] checkitemIds){
		try {
			checkGroupService.add(checkGroup,checkitemIds);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,MessageConstant.ADD_CHECKGROUP_FAIL);
		}
		return new Result(true,MessageConstant.ADD_CHECKGROUP_SUCCESS);
	}

	@RequestMapping("/findPage")
	public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
		return checkGroupService.QueryPage(
				queryPageBean.getCurrentPage(),
				queryPageBean.getPageSize(),
				queryPageBean.getQueryString()
		);
	}

	@RequestMapping("/findById")
	public Result findById(Integer id){
		CheckGroup checkGroup = checkGroupService.findById(id);
		if (checkGroup!=null){
			return new Result(true,MessageConstant.QUERY_CHECKGROUP_SUCCESS,checkGroup);
		}
		return new Result(false,MessageConstant.QUERY_CHECKGROUP_FAIL);
	}

	@RequestMapping("/findCheckItemIdsByCheckGroupId")
	public List<Integer> findCheckItemIdsByCheckGroupId(Integer id){
		List<Integer> list = checkGroupService.findCheckItemIdsByCheckGroupId(id);
		return list;
	}

	@RequestMapping("/edit")
	public Result edit(@RequestBody CheckGroup checkGroup , Integer[] checkitemIds){
		try {
			checkGroupService.edit(checkGroup,checkitemIds);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false,MessageConstant.EDIT_CHECKGROUP_FAIL);
		}
		return new Result(true,MessageConstant.EDIT_CHECKGROUP_SUCCESS);
	}

	@RequestMapping("/findAll")
	public List<CheckGroup> findAll(){
		List<CheckGroup> list = checkGroupService.findAll();
		if (list!=null && list.size()>0){
			return list;
		}
		return null;
	}
}
