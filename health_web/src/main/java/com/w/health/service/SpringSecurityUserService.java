package com.w.health.service;


import com.alibaba.dubbo.config.annotation.Reference;
import com.w.health.domain.Permission;
import com.w.health.domain.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class SpringSecurityUserService implements UserDetailsService {

	@Reference//注意L此处要通过dubbo远程调用用户服务
	private UserService userService;

	/**
	 * 根据用户名查询用户信息
	 *
	 * @param
	 * @return
	 * @throws UsernameNotFoundException
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//远程调用用户服务,根据用户名查询用户信息
		com.w.health.domain.User user = userService.findUserByUsername(username);
		if (user == null) {
			//用户名不存在,抛出异常
			return null;
		}
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();

		Set<Role> roles = user.getRoles();
		for (Role role : roles) {
			Set<Permission> permissions = role.getPermissions();
			for (Permission permission : permissions) {
				//授权
				list.add(new SimpleGrantedAuthority(permission.getKeyword()));
			}
		}
			/**
			 * User()
			 * 1.指定用户名.
			 * 2.指定密码
			 * 3.传递授予角色和权限
			 */
			UserDetails userDetails = new User(username, user.getPassword(), list);
			return userDetails;

	}
}
